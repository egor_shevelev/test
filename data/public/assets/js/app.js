function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";

    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

$(document).ready(function () {
    $('#btn-login').on('click', function () {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/auth',
            data: {
                email: $('#input-email').val(),
                password: $('#input-password').val()
            },
            success: function (data) {
                if (data.token !== "false") {
                    $('#dialog_auth').dialog('close');
                    celendar_run();
                }
            }
        })
    });
    $('#btn-new-event').on('click', function () {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/create-events',
            data: {
                name: $('#input-new-event-name').val(),
                date: $('#input-new-event-date').val(),
                time: $('#input-new-event-time').val(),
                description: $('#input-new-event-description').val(),
                color: $('#input-new-event-color').val(),
            },
            success: function (data) {
                if (data.data === "true") {
                    $('#dialog-new-event').dialog('close');
                    $('#calendar').fullCalendar('refetchEvents');
                } else {
                    alert('Save error');
                }
            }
        })
    });

    $('#btn-edit-event').on('click', function () {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/update-events',
            data: {
                id: $('#input-edit-event-id').val(),
                name: $('#input-edit-event-name').val(),
                date: $('#input-edit-event-date').val(),
                time: $('#input-edit-event-time').val(),
                description: $('#input-edit-event-description').val(),
                color: $('#input-edit-event-color').val(),
            },
            success: function (data) {
                if (data.data === "true") {
                    $('#dialog-edit-event').dialog('close');
                    $('#calendar').fullCalendar('refetchEvents');
                } else {
                    alert('Update error');
                }
            }
        })
    });

    $('#btn-delete-event').on('click', function () {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/delete-events',
            data: {
                id: $('#input-edit-event-id').val()
            },
            success: function (data) {
                if (data.data === "true") {
                    $('#dialog-edit-event').dialog('close');
                    $('#calendar').fullCalendar('refetchEvents');
                } else {
                    alert('Delete error');
                }
            }
        })
    });

});

function celendar_run() {
    $('#calendar').fullCalendar({
        defaultDate: '2017-08-4',
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: {
            url: '/events',
        },
        customButtons: {
            myCustomButton: {
                text: 'Add event',
                click: function () {
                    $('#dialog-new-event').dialog();
                }
            }
        },
        header: {
            left: 'prev,next myCustomButton',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        eventClick: function (calEvent, jsEvent, view) {
            console.log(calEvent);
            if (calEvent.access) {
                $('#input-edit-event-id').val(calEvent.id);
                $('#input-edit-event-name').val(calEvent.title);
                $('#input-edit-event-date').val(calEvent.date);
                $('#input-edit-event-time').val(calEvent.datetimevalue);
                $('#input-edit-event-description').val(calEvent.description);
                $('#input-edit-event-color').val(calEvent.color);
                $('#dialog-edit-event').dialog({
                    'title': calEvent.title
                });
            } else {
                $('#read-event').html('Date: ' + calEvent.start + '<br> Description: ' + calEvent.description);
                $('#read-event').dialog({
                    'title': calEvent.title + ' (Read mode)'
                })
            }
            // change the border color just for fun
            $(this).css('border-color', 'red');

        },
        eventDrop: function (event, delta, revertFunc) {

            alert(event.title + " was dropped on " + event.start.format());

            if (!confirm("Are you sure about this change?")) {
                revertFunc();
            }

        }

    });
}

