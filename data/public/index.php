<?php
ini_set('display_errors', true);
require_once __DIR__ . '/../vendor/autoload.php';
try{
    $app = new \Library\App();
    $app->run();
} catch (Exception $e) {
    die($e->getMessage());
}
