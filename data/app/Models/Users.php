<?php
/**
 * Created by PhpStorm.
 * User: egor
 * Date: 02.08.17
 * Time: 19:21
 */
namespace Models;

use Library\ModelBase;

class Users extends ModelBase
{
    protected $table = 'users';

    protected $id;
    protected $email;
    protected $password;
    protected $is_admin;
    protected $token;
    protected $invite_token;


    public static function password($password)
    {
        return md5($password);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsAdmin()
    {
        return $this->is_admin;
    }

    /**
     * @param mixed $is_admin
     * @return $this
     */
    public function setIsAdmin($is_admin)
    {
        $this->is_admin = $is_admin;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInvitetoken()
    {
        return $this->invite_token;
    }

    /**
     * @param mixed $invite_token
     * @return $this
     */
    public function setInvitetoken($invite_token)
    {
        $this->invite_token = $invite_token;
        return $this;
    }


}