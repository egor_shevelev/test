<?php
/**
 * Created by PhpStorm.
 * User: egorshevelev
 * Date: 04.08.17
 * Time: 1:55
 */

namespace Library;


use Models\Users;

class Security
{
    protected static $instance = null;

    protected $settings = [];

    /** @var  Users */
    protected $user;
    protected $isGuest;

    const GRANT_AUTH = 'GRANT_AUTH';

    public static function init($config)
    {
        if (!self::$instance instanceof Security) {
            self::$instance = new self($config);
        }

        return self::$instance;
    }

    public function __construct($config)
    {
        $this->settings = $config;
        $this->initUser();
    }

    private function initUser()
    {
        $userId = isset($_COOKIE['user_id']) ? $_COOKIE['user_id'] : null;
        $token = isset($_COOKIE['token']) ? $_COOKIE['token'] : null;

        if (is_null($userId) or is_null($token)) {
            $this->setIsGuest(true);
        } else {
            $this->checkUser($userId, $token);
            $this->setIsGuest(false);
        }
    }

    public function auth($email, $password)
    {
        $user = Users::getOne(['email' => $email, 'password' => Users::password($password)]);
        if (!$user->getId()) {
            return false;
        }
        $user->setToken($this->createToken())
            ->save();

        setcookie('user_id', $user->getId());
        setcookie('token', $user->getToken());

        return $user->getToken();
    }

    protected function createToken()
    {
        return bin2hex(microtime() / rand(1, 999));
    }

    protected function checkUser($userId, $token)
    {
        $user = Users::getOne(['id' => $userId, 'token' => $token]);

        if (!$user->getId()) {
            unset($_COOKIE['user_id']);
            unset($_COOKIE['token']);
        }

        $this->user = $user;
    }


    public function check($controller, $action)
    {
        $settings = $this->getSettings();

        if (!key_exists($action, $settings[$controller])) {
            return false;
        }

        return true;
    }

    /**
     * @return null|Users
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function isGuest()
    {
        return $this->isGuest;
    }

    /**
     * @param mixed $isGuest
     */
    public function setIsGuest($isGuest)
    {
        $this->isGuest = $isGuest;
    }

    /**
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param array $settings
     */
    public function setSettings(array $settings)
    {
        $this->settings = $settings;
    }


}