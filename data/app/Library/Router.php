<?php
/**
 * Created by PhpStorm.
 * User: egor
 * Date: 03.08.17
 * Time: 19:27
 */

namespace Library;

/**
 * Class Router
 * @package Library
 */
class Router
{
    protected $routes = [];

    protected $defaultController = 'index';
    protected $defaultAction = 'index';
    protected $defaultMethod = 'GET';
    protected $URIpattern = '/<([a-zA-Z0-9_]*)>/';
    protected $notFound = 'notFound';

    protected $requestUrl;

    public function __construct($config)
    {
        $this->mapConfig($config);
    }

    private function mapConfig($config)
    {
        foreach ($config as $uri => $params) {
            $url = $uri;
            $method = isset($params['method']) ? strtoupper($params['method']) : $this->defaultMethod;
            $this->routes[$method][$this->createPattern($url)] = [
                'controller' => isset($params['controller']) ? $params['controller'] : $this->defaultController,
                'action' => isset($params['action']) ? $params['action'] : $this->defaultAction,
                'method' => $method,
//                'params' => $this->getParams($url)
            ];
        }
    }

    private function createPattern($url)
    {
        return preg_replace($this->URIpattern, '*', $url);
    }

    private function getParams($uri)
    {
        $result = [];
        preg_match_all($this->URIpattern, $uri, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            $result[] = $match[1];
        }
        return $result;
    }

    public function getAction()
    {
        $requestMethod = (
            isset($_POST['_method'])
            && ($_method = strtoupper($_POST['_method']))
            && in_array($_method, ['PUT', 'DELETE'], true)
        ) ? $_method : $_SERVER['REQUEST_METHOD'];
        $requestUrl = $_SERVER['REQUEST_URI'];
        // strip GET variables from URL
        if (($pos = strpos($requestUrl, '?')) !== false) {
            $requestUrl = substr($requestUrl, 0, $pos);
        }
        return $this->findRoute($requestUrl, $requestMethod);
    }

    protected function findRoute($requestUrl, $requestMethod)
    {
        $this->requestUrl = $requestUrl;

        $requestUrl = $this->createPattern($requestUrl);

        $result = [
            'controller' => $this->defaultController,
            'action' => $this->notFound,
            'method' => $this->defaultMethod,
            'params' => []
        ];
        if(key_exists($requestUrl, $this->routes[$requestMethod])) {
            $result = $this->routes[$requestMethod][$requestUrl];
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getRequestUrl()
    {
        return $this->requestUrl;
    }

    /**
     * @param mixed $requestUrl
     */
    public function setRequestUrl($requestUrl)
    {
        $this->requestUrl = $requestUrl;
    }


//    public function match($requestUrl = null, $requestMethod = null) {
//        $params = array();
//        $match = false;
//        // set Request Url if it isn't passed as parameter
//        if($requestUrl === null) {
//            $requestUrl = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
//        }
//        // strip base path from request url
//        $requestUrl = substr($requestUrl, strlen($this->basePath));
//        // Strip query string (?a=b) from Request Url
//        if (($strpos = strpos($requestUrl, '?')) !== false) {
//            $requestUrl = substr($requestUrl, 0, $strpos);
//        }
//        // set Request Method if it isn't passed as a parameter
//        if($requestMethod === null) {
//            $requestMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';
//        }
//
//        foreach($this->routes as $handler) {
//            list($methods, $route, $target, $name) = $handler;
//            $method_match = (stripos($methods, $requestMethod) !== false);
//            // Method did not match, continue to next route.
//            if (!$method_match) continue;
//            if ($route === '*') {
//                // * wildcard (matches all)
//                $match = true;
//            } elseif (isset($route[0]) && $route[0] === '@') {
//                // @ regex delimiter
//                $pattern = '`' . substr($route, 1) . '`u';
//                $match = preg_match($pattern, $requestUrl, $params) === 1;
//            } elseif (($position = strpos($route, '[')) === false) {
//                // No params in url, do string comparison
//                $match = strcmp($requestUrl, $route) === 0;
//            } else {
//                // Compare longest non-param string with url
//                if (strncmp($requestUrl, $route, $position) !== 0) {
//                    continue;
//                }
//                $regex = $this->compileRoute($route);
//                $match = preg_match($regex, $requestUrl, $params) === 1;
//            }
//            if ($match) {
//                if ($params) {
//                    foreach($params as $key => $value) {
//                        if(is_numeric($key)) unset($params[$key]);
//                    }
//                }
//                return array(
//                    'target' => $target,
//                    'params' => $params,
//                    'name' => $name
//                );
//            }
//        }
//        return false;
//    }
}