<?php
/**
 * Created by PhpStorm.
 * User: egorshevelev
 * Date: 04.08.17
 * Time: 0:14
 */
namespace Library;

/**
 * Class App
 * @package Library
 */
class App
{
    /** @var  Router */
    public static $route;

    /** @var  Config */
    public static $config;

    /** @var  Security */
    public static $security;

    /** @var  \PDO */
    public static $database;

    /** @var  Request */
    public static $request;

    protected $currentController;
    protected $currentAction;

    /** @var  App */
    public static $application;

    public function __construct()
    {
        $this->initConfig();
        $this->initDatabase();
        $this->initRoute();
        $this->initSecurity();
        $this->initRequest();
        App::$application = $this;
    }

    private function initRequest()
    {
        App::$request = Request::init();
    }

    private function initDatabase()
    {
        $dbConfig = App::$config->database;
        try {
            App::$database = new \PDO($dbConfig['dsn'], $dbConfig['user'], $dbConfig['password']);
        } catch (\PDOException $e) {
            throw new \Exception('Database error');
        }
    }

    private function initSecurity()
    {
        App::$security = Security::init(App::$config->security);
    }

    private function initConfig()
    {
        App::$config = Config::init();
    }

    private function initRoute() {
        App::$route = new Router(App::$config->router);
    }

    public function run()
    {
        $handler = App::$route->getAction();

        $controller = $this->getControllerObject($handler['controller']);

        $method = $this->getControllerMethod($controller, $handler['action']);

        try {
            call_user_func([$controller, 'beforeAction']);
            $result = call_user_func([$controller, $method], []);
            $this->view($result);
        } catch (\Exception $e) {
            echo 'Fatal error: ' . $e->getMessage();
        }
    }

    protected function view(array $result)
    {
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    private function getControllerMethod(ControllerBase $controller, $action)
    {
        $this->setCurrentAction($action);
        $action = $action . 'Action';
        if(!method_exists($controller, $action)) {
            throw new \Exception("Action " . $action . " not found");
        }

        return $action;
    }

    private function getControllerObject($name)
    {
        $this->setCurrentController('Controllers\\' . ucfirst($name));
        $name = 'Controllers\\' . ucfirst($name) . 'Controller';
        if(!class_exists($name)) {
            throw new \Exception("Class " . $name . " can't be load");
        }

        return new $name();
    }

    /**
     * @return mixed
     */
    public function getCurrentController()
    {
        return $this->currentController;
    }

    /**
     * @param mixed $currentController
     */
    public function setCurrentController($currentController)
    {
        $this->currentController = $currentController;
    }

    /**
     * @return mixed
     */
    public function getCurrentAction()
    {
        return $this->currentAction;
    }

    /**
     * @param mixed $currentAction
     */
    public function setCurrentAction($currentAction)
    {
        $this->currentAction = $currentAction;
    }
}