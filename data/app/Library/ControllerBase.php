<?php
/**
 * Created by PhpStorm.
 * User: egor
 * Date: 02.08.17
 * Time: 19:25
 */
namespace Library;

class ControllerBase
{
    public function beforeAction()
    {
        if(!App::$security->check(App::$application->getCurrentController(), App::$application->getCurrentAction())){
            http_response_code(403);
            throw new \Exception('Access denied');
        }
    }
}