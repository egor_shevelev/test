<?php
/**
 * Created by PhpStorm.
 * User: egor
 * Date: 02.08.17
 * Time: 19:28
 */

namespace Library;

/**
 * Class Config
 * @package Library
 *
 * @property $router
 */
class Config
{
    /** @var  string config file name */
    protected $configName = 'app';

    protected $config = [];

    protected static $instance = null;

    const APP_CONFIG = 'config';

    /**
     * Config constructor.
     * @param string $fileName
     */
    public function __construct(string $fileName = null)
    {
        if (!is_null($fileName)) {
            $this->configName = $fileName;
        }

        $this->load();
    }


    public static function init()
    {
        if (!self::$instance instanceof Config) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function load()
    {
        if (file_exists($this->filePath())) {
            $this->config = include $this->filePath();
        } else {
            throw new \Exception('Config file not found');
        }
    }

    protected function filePath()
    {
        return __DIR__ . '/../' . self::APP_CONFIG . '/' . $this->getConfigName() . '.php';
    }

    /**
     * @return mixed
     */
    public function getConfigName()
    {
        return $this->configName;
    }

    public function __get($name)
    {
        if (isset($this->config[$name])) {
            return $this->config[$name];
        }

        return null;
    }

}