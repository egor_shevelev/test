<?php
/**
 * Created by PhpStorm.
 * User: egor
 * Date: 02.08.17
 * Time: 19:22
 */

namespace Library;

class ModelBase
{
    /** @var  string Database table name */
    protected $table;

    protected $fields;

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    public function __construct()
    {
        $this->fields = $this->getDatabaseColumn();
    }

    /**
     * Get list
     * @param array|null $params
     * @return array
     */
    public static function getList(array $params = null)
    {
        $calledClass = get_called_class();
        /** @var ModelBase $object */
        $object = new $calledClass();

        if(is_null($params)) {
            $db = App::$database->prepare('select * from ' . $object->getTable());
            $db->execute();
        } else {
            list($where, $bind) = $object->prepareWhereParams($params);
            $db = App::$database->prepare('select * from ' . $object->getTable() . ' where ' . $where);
            $object->bindValues($bind, $db);
        }
        return $object->listObjects($db);
    }

    protected function listObjects(\PDOStatement $db)
    {
        $result = [];
        while ($row = $db->fetch(\PDO::FETCH_ASSOC))
        {
            $result[] = $row;
        }
        return $result;
    }

    protected function bindValues(array $bind, \PDOStatement &$db)
    {
        foreach ($bind as $key => $value)
        {
            $db->bindValue($key, $value);
        }
    }

    protected function prepareWhereParams(array $params)
    {
        $resultCondition = [];
        $bind = [];
        foreach ($params as $key => $value)
        {
            $resultCondition[] = $key . ' = :' . $key;
            $bind[':'.$key] = $value;
        }

        return [implode(' AND ', $resultCondition), $bind];
    }

    /**
     * Get one by ID
     * @param integer|array $param - primary key
     * @return ModelBase
     */
    public static function getOne($param)
    {
        $calledClass = get_called_class();
        /** @var ModelBase $object */
        $object = new $calledClass();

        if(!is_array($param)) {
            $db = App::$database->prepare('select * from ' . $object->getTable() . ' where id = :id LIMIT 1');
            $db->bindValue(':id', $param);
        } else {
            list($where, $bind) = $object->prepareWhereParams($param);
            $db = App::$database->prepare('select * from ' . $object->getTable() . ' where ' . $where . ' LIMIT 1');
            $object->bindValues($bind, $db);
        }

        $db->execute();
        $row = $db->fetch(\PDO::FETCH_ASSOC);
        return $object->mapToObject($row);
    }

    /**
     * Delete item
     * @return bool
     */
    public function delete()
    {
        $db = App::$database->prepare('delete from ' . $this->getTable() . ' where id = :id LIMIT 1');
        $db->bindValue(':id', $this->getId());

        if(!$db->execute()) {
            return false;
        }

        return true;
    }

    protected function mapToObject($row)
    {
        foreach ($this->fields as $field) {
            $setter = $this->getSetterName($field);
            if (!method_exists($this, $setter)) {
                throw new \Exception('Method ' . $setter . ' not exist');
            }
            $this->$setter($row[$field]);
        }
        return $this;
    }

    /**
     * Save model
     * @return bool
     */
    public function save()
    {
        if (is_null($this->getId())) {
            $db = App::$database->prepare('insert into ' . $this->getTable() . $this->getInsertFields() . ' values ' . $this->getBindsName());
            $this->bindParams($db);
        } else {
            $db = App::$database->prepare('update ' . $this->getTable() . ' set ' . $this->getUpdateFields() . ' where id = :id');
            $this->bindParams($db, true);
        }
        if (!$db->execute()) {
            return false;
        }
        return true;
    }

    private function getUpdateFields()
    {
        $fields = $this->unsetIdField();
        array_walk($fields, function (&$field) {
            $field = $field . ' = :' . $field;
        });
        return implode(', ', $fields);
    }

    public function getId()
    {
        throw new \Exception("getId() must be overload");
    }

    private function bindParams(\PDOStatement &$db, $withId = false)
    {
        if (!$withId) {
            $fields = $this->unsetIdField();
        } else {
            $fields = $this->fields;
        }
        foreach ($fields as $field) {

            $getter = $this->getGetterName($field);
            if (!method_exists($this, $getter)) {
                throw new \Exception('Method ' . $getter . ' not exist');
            }

            $db->bindValue(':' . $field, $this->$getter());
        }
    }

    private function getGetterName($name)
    {
        return preg_replace_callback('/(_.|-.)/', function ($matches) {
            return strtoupper($matches[0][1]);
        }, 'get' . $name);
    }

    private function getSetterName($name)
    {
        return preg_replace_callback('/(_.|-.)/', function ($matches) {
            return strtoupper($matches[0][1]);
        }, 'set' . ucfirst($name));
    }

    private function getBindsName()
    {
        $fields = $this->unsetIdField();

        array_walk($fields, function (&$field) {
            $field = ':' . $field;
        });
        return '(' . implode(', ', $fields) . ')';
    }

    private function getInsertFields()
    {
        $fields = $this->unsetIdField();

        array_walk($fields, function (&$field) {
            $field = '`' . $field . '`';
        });
        return '(' . implode(', ', $fields) . ')';
    }

    private function unsetIdField()
    {
        $fields = $this->fields;

        if (in_array('id', $fields)) {
            unset($fields[array_search('id', $fields)]);
        }

        return $fields;
    }

    private function getDatabaseColumn()
    {
        $sth = App::$database->prepare('SHOW COLUMNS FROM ' . $this->getTable());
        $sth->execute();

        $result = [];
        while ($row = $sth->fetch()) {
            $result[] = $row['Field'];
        }
        return $result;

    }
}