<?php
/**
 * Created by PhpStorm.
 * User: egor
 * Date: 07.08.17
 * Time: 13:37
 */

namespace Library;

/**
 * Class Request
 * initialization and mapping request data
 * @package Library
 */
class Request
{
    protected $POST = [];
    protected $GET = [];
    protected $headers = [];

    public function __construct()
    {
        $this->set();
        $this->set('GET');
        $this->getHeaders();
    }

    public static function init()
    {
        return new self();
    }

    protected function getHeaders()
    {
        $this->headers = getallheaders();
    }

    protected function set($method = 'POST')
    {
        if($method == 'POST'){
            $data = $_POST;
        } else {
            $data = $_GET;
        }
        if(is_array($data)) {
            $methodName = 'add'. $method;
            foreach ($data as $name => $value) {
                $this->$methodName($name, $value);
            }
        }
    }

    /**
     * Get variable in GET
     * @param null $name
     * @return array|mixed|null
     */
    public function get($name = null)
    {
        if(is_null($name)){
            return $this->GET;
        } else {
            return key_exists('name', $this->GET) ? $this->GET[$name] : null;
        }
    }

    /**
     * Get variable in POST
     * @param null $name
     * @return array|mixed|null
     */
    public function post($name = null)
    {
        if(is_null($name)){
            return $this->POST;
        } else {
            return key_exists($name, $this->POST) ? $this->POST[$name] : null;
        }
    }

    /**
     * Get header
     * @param $name
     * @return array|mixed|null
     */
    public function header($name)
    {
        if(is_null($name)){
            return $this->headers;
        } else {
            return key_exists($name, $this->headers) ? $this->headers[$name] : null;
        }
    }

    public function addPOST($name, $value) {
        $this->POST[$name] = $value;
    }

    public function addGET($name, $value) {
        $this->GET[$name] = $value;
    }
}