<?php
/**
 * Created by PhpStorm.
 * User: egor
 * Date: 02.08.17
 * Time: 19:23
 */

return [
    'database' => [
        'dsn' => 'mysql:host=localhost;dbname=test_app',
        'user' => 'root',
        'password' => '123qwe'
    ],
    'router' => [
        '/auth' => [
            'method' => 'POST',
            'controller' => 'index',
            'action' => 'auth'
        ],
        '/events' => [
            'controller' => 'events',
            'action' => 'list'
        ],
        '/create-events' => [
            'method' => 'POST',
            'controller' => 'events',
            'action' => 'create',
        ],
        '/update-events' => [
            'method' => 'POST',
            'controller' => 'events',
            'action' => 'update',
        ],
        '/delete-events' => [
            'method' => 'POST',
            'controller' => 'events',
            'action' => 'delete',
        ],
    ],
    'security' => [
        'Controllers\\Events' => [
            'list' => 'GRANT_AUTH',
            'create' => 'GRANT_AUTH',
            'update' => 'GRANT_AUTH',
            'delete' => 'GRANT_AUTH'
        ]
    ]
];