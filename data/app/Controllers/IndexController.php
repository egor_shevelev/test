<?php
/**
 * Created by PhpStorm.
 * User: egorshevelev
 * Date: 04.08.17
 * Time: 0:44
 */

namespace Controllers;

use Library\App;
use Library\ControllerBase;
use Models\Users;

class IndexController extends ControllerBase
{

    public function authAction()
    {
        if(!App::$security->isGuest()){
            return ['data' => 'already login'];
        }

        $email = App::$request->post('email');
        $password = App::$request->post('password');

        $token = App::$security->auth($email, $password);

        return ['token' => $token];
    }

    public function notFoundAction()
    {
        http_response_code(404);
        return ['data' => 'Not found'];
    }
}