<?php
/**
 * Created by PhpStorm.
 * User: egor
 * Date: 04.08.17
 * Time: 13:34
 */

namespace Controllers;


use Library\App;
use Library\ControllerBase;
use Models\Events;

class EventsController extends ControllerBase
{
    public function listAction()
    {
        $result = [];
        $events = Events::getList();
        if (count($events)) {
            /** @var Events $event */
            foreach ($events as $event) {
                $result[] = [
                    'id' => $event['id'],
                    'title' => $event['name'],
                    'start' => $event['date'],
                    'date' => $event['date'],
                    'datetimevalue' => $event['time'],
                    'description' => $event['description'],
                    'author' => $event['author'],
                    'access' => App::$security->getUser()->getId() == $event['author'] ? true : (bool) App::$security->getUser()->getIsAdmin()
                ];
            }
        }
        return $result;
    }

    public function createAction()
    {
        $name = App::$request->post('name');
        $date = App::$request->post('date');
        $time = App::$request->post('time');
        $description = App::$request->post('description');
        $color = App::$request->post('color');

        $event = new Events();
        $event->setName($name)
            ->setDate($date)
            ->setTime($time)
            ->setDescription($description)
            ->setColor($color)
            ->setAuthor(App::$security->getUser()->getId());

        if (!$event->save()) {
            return ['data' => 'false'];
        }

        return ['data' => 'true'];
    }

    public function updateAction()
    {
        $id = App::$request->post('id');
        if(is_null($id)) {
            return ['data' => 'false'];
        }
        $event = Events::getOne($id);

        if(!App::$security->getUser()->getIsAdmin() && $event->getAuthor() != App::$security->getUser()->getId()) {
            return ['data' => 'false'];
        }

        $name = App::$request->post('name');
        $date = App::$request->post('date');
        $time = App::$request->post('time');
        $description = App::$request->post('description');
        $color = App::$request->post('color');

        if(!$event) {
            return ['data' => 'false'];
        }
        $event->setName($name)
            ->setDate($date)
            ->setTime($time)
            ->setDescription($description)
            ->setColor($color)
            ->setAuthor(App::$security->getUser()->getId());

        if (!$event->save()) {
            return ['data' => 'false'];
        }

        return ['data' => 'true'];
    }

    public function deleteAction()
    {
        $id = App::$request->post('id');

        if(is_null($id)) {
            return ['data' => 'false'];
        }
        $event = Events::getOne($id);

        if(!App::$security->getUser()->getIsAdmin() && $event->getAuthor() != App::$security->getUser()->getId()) {
            return ['data' => 'false'];
        }

        if(!$event->delete()) {
            return ['data' => 'false'];
        }
        return ['data' => 'true'];
    }

}