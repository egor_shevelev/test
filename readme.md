Install
-------------------
For installation we must generate autoload: 
````
cd data
composer install
````

If you using VagrantL: 
``vagrant up``
and add new host in /ets/hosts ``192.168.33.33 test.dev``

Mysql dump ``data/test_app.sql``

Enter page ``http://test.dev/index.html``

Notice
-------------------

Backend is made from scratch.
Frontend is made with implement jQuery.

Folder /deploy contains a mySql dump and a config for Apache.

Admin user:

admin@test.dev 123456
user@test.dev 123456

PS
-----
Unfortunately, there was not much time to develop, there was not enough time. I think the work done can demonstrate my skills and experience.
