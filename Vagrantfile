Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/trusty64"

  config.vm.network "private_network", ip: "192.168.33.77"

  config.vm.synced_folder "./", "/var/www/test", owner: "www-data"

  config.vm.provider "virtualbox" do |vb|
     vb.memory = "1024"
  end

  config.vm.provision "shell", inline: <<-SHELL

    sudo su
    locale-gen ru_RU.UTF-8
    apt-get update -y

    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

    # Install and configure apache2
    apt-get install -y apache2
    cp -R /var/www/test/deploy/apache2/* /etc/apache2/sites-available
    a2ensite test.conf
    a2enmod rewrite
    a2enmod headers


    # Install and configure php5
    apt-get install -y php7 2> /dev/null
    apt-get install -y php7-cli 2> /dev/null
    apt-get install -y php7-mysql 2> /dev/null
    apt-get install -y php7-mcrypt 2> /dev/null
    apt-get install -y php7-gd 2> /dev/null
    apt-get install -y php7-curl 2> /dev/null
    apt-get install -y php7-xdebug 2> /dev/null
    sed -i 's/display_errors = Off/display_errors = On/g' /etc/php5/apache2/php.ini

    # Install, configure and migrate MySQL
    debconf-set-selections <<< 'mysql-server mysql-server/root_password password 123qwe'
    debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password 123qwe'
    apt-get install -y mysql-client-core-5.6 2> /dev/null
    apt-get install -y mysql-client-5.6 2> /dev/null
    apt-get install -y mysql-server-5.6 2> /dev/null
    cp /var/www/test/deploy/my.cnf /etc/mysql/
    service mysql restart

    # Install PhpMyAdmin
    debconf-set-selections <<< 'phpmyadmin phpmyadmin/dbconfig-install boolean true'
    debconf-set-selections <<< 'phpmyadmin phpmyadmin/app-password-confirm password 123qwe'
    debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/admin-pass password 123qwe'
    debconf-set-selections <<< 'phpmyadmin phpmyadmin/mysql/app-pass password 123qwe'
    debconf-set-selections <<< 'phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2'
    apt-get install -y phpmyadmin 2> /dev/null


    # Update vendors
    cd /var/www/test/data
    composer update


    cat << EOF

OK

EOF

  SHELL

end