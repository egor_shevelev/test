-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 07, 2017 at 05:25 PM
-- Server version: 5.6.33-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(160) COLLATE utf8_bin NOT NULL,
  `date` date NOT NULL,
  `time` time DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_bin NOT NULL,
  `author` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `color` varchar(6) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `date`, `time`, `description`, `author`, `status`, `color`) VALUES
(1, 'Test', '2017-08-09', '00:00:00', 'description', 3, 1, ''),
(2, '111', '2017-08-11', '00:00:00', 'description', 3, NULL, '#00000'),
(3, '222', '2017-08-03', '00:00:00', 'description', 4, NULL, '#d1818'),
(4, '333', '2017-08-04', '00:00:00', 'description', 3, NULL, '#00000'),
(5, '555', '2017-08-05', '00:00:00', 'description', 3, NULL, '#00000');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(160) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `is_admin` tinyint(4) NOT NULL,
  `token` varchar(36) COLLATE utf8_bin DEFAULT NULL,
  `invite_token` varchar(36) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `is_admin`, `token`, `invite_token`) VALUES
(3, 'admin@test.dev', 'e10adc3949ba59abbe56e057f20f883e', 1, '302e30303033343031353933303338383232', NULL),
(4, 'user@test.dev', 'e10adc3949ba59abbe56e057f20f883e', 1, '302e30303033343031353933303338383232', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
